# mhSlider
Responsive jQuery Content-Slider

Dieses Javascript-Plugin für Webseiten ermöglicht eine responsive Darstellung von Inhalten mit schönem Übergangseffekt oder als Karussell.

# Changelog
V 2.3
  - Inhalts-Karussell überarbeitet

V 2.2
  - Unterstützung für multiple Slider

V 2.1
  - komplettes Umschreiben des Codes für modernere Umgebungen
  - Unterstützung für weitere Inhalte im Karussell

V 2.0
  - Fehlerbehebungen
  - Trigger Funktion goto() eingeführt

V 1.9
  - Fehlerbehebung bei Einzelbildern

V 1.8:
  - Fehlerbehebungen bei den Übergangseffekten

V 1.7:
  - Callback Funktion drawn hinzugefügt
  - drawBackground Option um das Hintergrundbild auszuschalten, bei zu kleinen Bildern für den Container
  - minimale Verbesserungen

V 1.6:
  - Funktion für weiter und zurück hinzugefügt
  - Trigger Funktionen next() und prev() angelegt
  - Minimierte Version hinzugefügt

V 1.5:
  - Fix für Internet Explorer
  
V 1.4:
  - Responsive-Background ist nun animiert.

V 1.3:
  - neue Features: imgMode (order,random), effect (fade, slide)

V 1.2:
  - Fehlerkorrekturen und Error-Handling

V 1.1:
  - neues Feature: duration (Dauer der Übergangsanimation)

V 1.0:
  - Erster Release
  - Features:
  	- Unkompliziertes Einbinden,
  	- KEIN Zusatzplugin für Javascript
  	- einfacher Aufruf
  	- einstellbare Größe
  	- Möglichkeit zum Pausieren des Sliders durch Mouseover
  	- einstellbarer Timer
  	
# Anleitung / How-To
- Download der Dateien mhSlider.js und mhSlider.css (oder die jeweils minimale Version)
- Einbinden in den Quellcode (HTML):

# Script einbinden:
Gegebenenfalls den Pfad zu der Datei anapssen
```html
<link rel="stylesheet" href="mhSlider.css">
<script type="text/javascript" src="mhSlider.js"></script>
```

# Struktur:
Bilder-Slider
```html
<div class="mhSlider">
  <img src="imgSrc1.jpg" alt="Alternativtext 1">
  <img src="imgSrc2.jpg" alt="Alternativtext 2">
  <img src="imgSrc3.jpg" alt="Alternativtext 3">
  <img src="imgSrc4.jpg" alt="Alternativtext 4">
  <img src="imgSrc5.jpg" alt="Alternativtext 5">
</div>
```
Inhalts-Karussell
```html
<div class="mhSlider" data-mhslider-duration="10" data-mhslider-direction="forwards|reverse" data-mhslider-mousetrigger="true|false">
  <div>Box 1</div>
  <div>Box 2</div>
  <div>Box 3</div>
  <div>Box 4</div>
  <div>Box 5</div>
</div>
```

# Script: (optional; nur wenn nicht die Klasse .mhSlider benutzt wird)
```html
<!-- Vor </body> -->
<script type="text/javascript">
jQuery(function($){
  var mhSlider = $('.mhSlider').mhSlider({
    contentWidth: 1920,
    contentHeight: 350,
    contentSortMode: 'order',
    effect: 'fade',
    timer: 5000,
    duration: 500,
    drawBackground: true,
    mouseTrigger: true,
    drawn: function(itemIdx) {},
  });

  // Funktionen für Bilder-Slider
  mhSlider.next();
  mhSlider.prev();
  mhSlider.goto(indexOfImg);
});
</script>
```

# Optionen für Bilder-Slider:
- contentWidth (INT) (optional): Gibt die Breite der Slider-Bilder an
- contentHeight (INT) (optional): Gibt die Höhe der Slider-Bilder an
- contentSortMode (STRING) (optional): Gibt die Reihenfolge der Slider-Bilder an (order=Wie Reihenfolge,random=Zufällig) | Default: order
- effect (STRING) (optional): Gibt den Effekt der Übergangsanimation an (fade=Überblenden,slide=zur Seite wischen) | Default: fade
- timer (INT) (optional): Gibt die Dauer bis zum Wechsel der Slider-Bilder an | Default: 5000 (ms)
- duration (INT) (optional): Gibt die Dauer der Übergangsanimation an | Default: 500 (ms)
- drawBackground (BOOL) (optional): Gibt an, ob ein leicht transparentes Hintergrundbild hinter den Slider gelegt werden soll, um bei kleineren Bildern den Container auszufüllen | Default: true
- mouseTrigger (BOOL) (optional): Gibt an, ob bei Mouseover der Wechsel der Slider-Bilder unterbrochen werden soll | Default: true
- drawn (FUNCTION) (optional): Dient als Rückgabe-Funktion um zu bestimmen, welches Element aktuell dargestellt wird
