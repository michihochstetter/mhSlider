/*! mhSlider v2.3 | (c) Michael Hochstetter | https://gitlab.com/michihochstetter/mhSlider/raw/master/LICENSE !*/
if (typeof jQuery == 'undefined') {
	if (typeof $ != 'function') {
		console.warn(
			'This Website needs a jQuery Library for mhSlider to work.'
		);
		jQuery = function() {};
	} else {
		jQuery = $;
	}
}
(function($) {
	$.fn.mhSlider = function(options) {

		// settings
		var _settings = $.extend(
			{
				contentWidth: 'auto',
				contentHeight: 'auto',
				contentSortMode: 'order',
				effect: 'fade',
				timer: 5000,
				duration: 500,
				drawBackground: true,
				mouseTrigger: true,
				drawn: function(){},
			},
			options || {}
		);

		$(this).each((idx, element) => {
			// vars
			var slider_intervals = [];
			var slider_container = $(element);
			if(slider_container.length===0) return;
			var slider_container_background = $('<span></span>');
			var slider_content = slider_container.children();
			if(slider_content.length==0) {
				console.warn("mhSlider: Es konnte kein Inhalt imn einem der Container gefunden werden.");
				return;
			}
			var slider_content_current_index = 0;
			var slider_effect_active = false;
			var slider_content_width = _settings.contentWidth;
			var slider_content_height = _settings.contentHeight;
			var slider_content_ratio = slider_content_width / slider_content_height;
			if(slider_content_width=="auto" || slider_content_height=="auto") {
				slider_content_width = slider_content[0].width;
				slider_content_height = slider_content[0].height;
				slider_content_ratio = slider_content_width / slider_content_height;
				slider_content_width = "100%";
				slider_content_height = $(document).width() / slider_content_ratio;
			}
			var slider_content_mode = _settings.contentSortMode;
			var slider_effect = _settings.effect;
			var slider_timer = _settings.timer;
			var slider_duration = _settings.duration;
			var slider_mouse_trigger = _settings.mouseTrigger;

			// set class
			if(!slider_container.hasClass('mhSlider')) slider_container.addClass('mhSlider');

			// mode
			switch(slider_content_mode) {
				case 'order':
					break;
				case 'random':
					slider_content = shuffle_array(slider_content);
					break;
				default:
					break;
			}

			var hasImgContent = slider_container.find('> img').length;
			if(hasImgContent) {
				if(window.matchMedia("(prefers-reduced-motion: reduce)").matches) return;

				function start_slider(timer) {
					if(timer===undefined) timer = slider_timer;
					slider_intervals[idx] = setInterval(function() {
						var slider_content_next_index = 0;
						if(slider_content_current_index!=slider_content.length-1) {
							slider_content_next_index = slider_content_current_index+1;
						}
						effect(slider_content_next_index);
						slider_content_current_index = slider_content_next_index;
					}, timer);
				}
				function stop_slider() {
					if(slider_intervals[idx]!=null) {
						clearInterval(slider_intervals[idx]);
					}
				}
				function effect(nextIndex = 0, prev = false, callback = () => {}) {
					slider_effect_active = true;
		
					if(_settings.drawBackground) {
						slider_container_background.css(
							'background-image', 'url('+slider_content.eq(nextIndex).prop('src')+')'
						);
					}
		
					switch(slider_effect) {
						case 'fade':
							slider_content.eq(slider_content_current_index).clearQueue().finish().animate({
								'opacity':'0',
								'z-index':'1'
							}, slider_duration);
							slider_content.eq(nextIndex).clearQueue().finish().animate({
								'opacity':'1',
								'z-index':'2'
							}, slider_duration, function() {
								slider_effect_active = false;
								callback();
							});
							_settings.drawn(nextIndex);
							break;
						case 'slide':
							if(!prev) {
								slider_content.eq(slider_content_current_index).clearQueue().finish().animate({
									'left':'-100%',
									'opacity':'0',
									'z-index':'1'
								}, slider_duration);
							} else {
								slider_content.eq(slider_content_current_index).clearQueue().finish().animate({
									'left':'100%',
									'opacity':'0',
									'z-index':'1'
								}, slider_duration);
							}
							slider_content.eq(nextIndex).css({'left':'50%'}).clearQueue().finish().animate({
								'opacity':'1',
								'z-index':'2'
							}, slider_duration, function() {
								slider_effect_active = false;
								callback();
							});
							_settings.drawn(nextIndex);
							break;
						default:
							break;
					}	
				}
				function get_container_height() {
					return slider_container.outerWidth() / (slider_content_ratio!=NaN ? slider_content_ratio : 1);
				}
				function set_container_height() {
					slider_container.css({'height':get_container_height()+'px'});
				}
				function next() {
					if(!slider_effect_active) {
						var slider_content_next_index = 0;
						if(slider_content_current_index!=slider_content.length-1) {
							slider_content_next_index = slider_content_current_index+1;
						}
						effect(slider_content_next_index);
						slider_content_current_index = slider_content_next_index;
						stop_slider();
						start_slider();
					}
				}
				function prev() {
					if(!slider_effect_active) {
						var slider_content_next_index = 0;
						if(slider_content_current_index==0) {
							slider_content_next_index = slider_content.length-1;
						} else {
							slider_content_next_index = slider_content_current_index-1;
						}
						effect(slider_content_next_index, true);
						slider_content_current_index = slider_content_next_index;
						stop_slider();
						start_slider();
					}
				}
				function goto(index = 0) {
					if(index==slider_content.length) index = 0;
					effect(index);
					slider_content_current_index = index;
					stop_slider();
					start_slider();
				}

				// init
				set_container_height();
				$(window).resize(function() {
					set_container_height();
				});
				slider_content.each(function() {
					$(this).css({
						'position':'absolute',
						'top':'0',
						'left':'50%',
						'transform':'translateX(-50%)',
						'opacity':'0',
						'z-index':'1',
						'max-width':'100%',
						'transition':'all 0s ease 0s'
					});
				});
				slider_container.css({
					'display':'block',
					'position':'relative',
					'overflow':'hidden',
					'width':'100%',
					'max-width':slider_content_width,
					'aspect-ratio':slider_content_ratio,
				});
				if(_settings.drawBackground) {
					slider_container.prepend(slider_container_background);
					slider_container_background.css({
						'position':'absolute',
						'top':'0',
						'left':'0',
						'width':'100%',
						'height':'100%',
						'background-size':'cover',
						'background-position':'center',
						'opacity':'.3',
						'background-repeat':'no-repeat',
						'z-index':'0',
						'transition':(slider_duration / 1000)+'s'
					});
				}
				slider_content.eq(slider_content_current_index).css({
					'opacity':'1',
					'z-index':'2'
				});
				if(_settings.drawBackground) {
					slider_container_background.css(
						'background-image', 'url('+slider_content.eq(slider_content_current_index).prop('src')+')'
					);
				}
				if(slider_content.length>1) start_slider();

				if(slider_mouse_trigger && slider_content.length>1) {
					slider_container.on('mouseenter', () => { stop_slider(); });
					slider_container.on('mouseleave', () => { start_slider(); });
				}
		
				// public functions
				var publicFunctions = {
					next: next,
					prev: prev,
					goto: goto,
				}
				element.mhSlider = publicFunctions;
			} else {
				if(window.matchMedia("(prefers-reduced-motion: reduce)").matches) return;

				if(slider_content.length>1) slider_container.wrapInner("<div class='mhSlider_inner'></div>");
				slider_content = $('.mhSlider_inner').children();

				$('.mhSlider_inner').append(slider_content.clone().attr('aria-hidden', true));

				if(!$.prototype.isInViewport) {
					$.fn.isInViewport = function() {
						var elementTop = $(this).offset().top;
						var elementBottom = elementTop + $(this).outerHeight();
						var viewportTop = $(window).scrollTop();
						var viewportBottom = viewportTop + $(window).height();
						return elementBottom > viewportTop && elementTop < viewportBottom;
					};
				}

				slider_container.attr('data-animated', false);
				if(slider_container.isInViewport()) slider_container.attr('data-animated', true);

				if(slider_container.data('mhslider-duration')) slider_container.css('--mhSlider-duration', slider_container.data('mhslider-duration')+'s');
				if(slider_container.data('mhslider-direction')=='reverse') slider_container.css('--mhSlider-direction', slider_container.data('mhslider-direction'));
				if(slider_container.data('mhslider-mousetrigger')==false) slider_mouse_trigger = false;

				if(slider_mouse_trigger && slider_content.length>1) {
					slider_container.on('mouseenter', () => { slider_container.attr('data-animated', false); });
					slider_container.on('mouseleave', (e) => {
						if(e.relatedTarget!=$('#mhLightbox_container').get(0)) slider_container.attr('data-animated', true);
					});
				}

				$(window).on('scroll', function() {
					if(slider_container.isInViewport()) slider_container.attr('data-animated', true);
					else slider_container.attr('data-animated', false);
				});
				slider_container.find('[mhlightbox]').on('click', function() {
					slider_container.attr('data-animated', false);
				});
				if($('#mhLightbox_container').length) {
					$(document).on('click', '#mhLightbox_close, #mhLightbox_container', function(e) {
						slider_container.attr('data-animated', true);
					});
				}
			}
		});

		// functions
		function shuffle_array(a) {
			var j, x, i;
			for (i = a.length; i; i--) {
				j = Math.floor(Math.random() * i);
				x = a[i - 1];
				a[i - 1] = a[j];
				a[j] = x;
			}
			return a;
		}
	};

	if($('.mhSlider').length) {
		$(window).on('load', function() {
			$('.mhSlider').mhSlider();
		});
	}
}(jQuery));